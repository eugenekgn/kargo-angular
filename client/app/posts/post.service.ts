import {Injectable} from 'angular2/core';
import {IPost} from './post';
import {Http, Response} from 'angular2/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class PostService {
    private _postUrl = 'http://jsonplaceholder.typicode.com/posts';

    constructor(private _http:Http) {

    }

    getPosts():Observable<IPost[]> {
        return this._http.get(this._postUrl).map((response:Response) =>
                <IPost[]>response.json())
            //.do(data => console.log('Data Stream ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    getPostDetails(id:number):Observable<IPost> {
        let postUrl = [this._postUrl, id].join('/');
        return this._http.get(postUrl)
            .map((response:Response) => <IPost>response.json());
    }

    private handleError(error:Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server Error');
    }
}