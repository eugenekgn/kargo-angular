import {Component, OnInit} from 'angular2/core';
import {RouteParams, Router} from 'angular2/router';
import {IPost} from './post';
import {PostService} from './post.service';
import { CapitalizePipe } from '../shared/capitalize.pipe';

@Component({
    templateUrl: './post-detail.component.html',
    pipes: [CapitalizePipe]
})

export class PostDetailComponent implements OnInit {
    pageTitle:string = 'Post Detail';
    post:IPost;
    errorMessage:string;


    constructor(private _postService:PostService,
                private _router:Router,
                private _routeParams:RouteParams) {
    }


    ngOnInit() {
        if (!this.post) {
            let id = +this._routeParams.get('id');
            this.getPostDetails(id);
        }
    }

    getPostDetails(id:number) {
        this._postService.getPostDetails(id)
            .subscribe(
                post => this.post = post,
                error => this.errorMessage = <any>error);
    }
    
    goBack():void {
        this._router.navigate(['Posts']);
    }
}