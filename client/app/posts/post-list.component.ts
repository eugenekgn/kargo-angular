import {Component, OnInit} from 'angular2/core';
import {PostService} from './post.service'
import {IPost} from './post';
import {PostFilterPipe} from './post-filter.pipe';
import {ROUTER_DIRECTIVES} from 'angular2/router';

@Component({
    selector: 'posts',
    templateUrl: './post-list.component.html',
    styleUrls: ['app/posts/post-list.component.css'],
    pipes: [PostFilterPipe],
    directives: [ROUTER_DIRECTIVES]
})

export class PostListComponent implements OnInit {
    pageTitle:string = 'Post List';
    listFilter:string;
    errorMessage:string;
    postsCollection:IPost[];

    constructor(private _postService:PostService) {
    }

    ngOnInit():void {
        this._postService.getPosts().subscribe(
            posts => this.postsCollection = posts,
            error => this.errorMessage = <any>error);
    }
}