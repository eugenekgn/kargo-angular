export interface IDataPoint {
    id:number;
    hits:number;
    date:string;
}
