import {Injectable} from 'angular2/core';
import {Http, Response} from 'angular2/http';
import {Observable} from 'rxjs/Observable';
import {IDataPoint} from './dataPoint'

@Injectable()
export class ChartService {
    private _postUrl = 'http://kargotest.herokuapp.com/api/trackers?';
    private _fromDate;
    private _toDate;

    constructor(private _http:Http) {

    }

    public getDataPoints(fromDate:string, toDate:string):Observable<IDataPoint[]> {

        let url = [this._postUrl, 'from=', fromDate, '&to=', toDate].join('');
        return this._http.get(url)
            .map(this.fixDates)
            .catch(this.handleError);
    }

    private fixDates(response:Response) {
        let data = response.json().data;

        if (!data || data.length === 0) {
            return [];
        }

        let dateCollection = [];
        let month = 0;
        let day = 0;

        //TODO: clean this up by breaking it into methods or use momentsjs, trying to save time
        data = data.sort((strDateA, strDateB) => {
            let dateA = Date.parse(strDateA.date);
            let dateB = Date.parse(strDateB.date);
            return new Date(dateA).getTime() - new Date(dateB).getTime();
        });

        //TODO: Move out to separate function
        let dayCountByMonth = {
            1: 31,
            2: 28,
            3: 31,
            4: 30,
            5: 31,
            6: 30,
            7: 31,
            8: 31,
            9: 30,
            10: 31,
            11: 30,
            12: 31
        };


        let firstEntry = data.shift();
        let dateParts = firstEntry.date.split('-');
        day = +dateParts[2];
        month = +dateParts[1];


        //Fix fromDate (Should fix for months as well, if month gap is missing)


        dateCollection.push(firstEntry);
        data.forEach((value, index)=> {

            let currDateParts = value.date.split('-');
            let currYear = +currDateParts[0];
            let currDay = +currDateParts[2];
            let currMonth = +currDateParts[1];

            //Same month
            if (currMonth === month) {
                day = day + 1;
                if (currDay === day) {
                    dateCollection.push(value);
                } else {
                    for (day; day < currDay; day++) {
                        dateCollection.push({
                            id: day,
                            hits: 0,
                            date: new Date(currYear, currMonth - 1, day).toISOString().split('T')[0]
                        });
                    }
                    dateCollection.push(value);
                }
            } else {

                //end of month filler
                let lastMonthEndDate = dayCountByMonth[month];

                if (day < lastMonthEndDate) {
                    for (day; day <= lastMonthEndDate; day++) {
                        dateCollection.push({
                            id: day,
                            hits: 0,
                            date: new Date(currYear, month - 1, day).toISOString().split('T')[0]
                        });
                    }
                }

                month = currMonth;
                day = 1;
                for (day; day < currDay; day++) {
                    dateCollection.push({
                        id: day,
                        hits: 0,
                        date: new Date(currYear, currMonth - 1, day).toISOString().split('T')[0]
                    });
                }
                dateCollection.push(value);
            }
        });


        //Fix toDate (Should fix for months as well, if month gap is missing)

        return dateCollection || [];
    }
    
    private handleError(error:Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server Error');
    }
}