import {Component, OnInit} from 'angular2/core';
import {RouteParams, Router} from 'angular2/router';
import {IDataPoint} from './dataPoint';
import {ChartService} from './chart.service';
import {ROUTER_DIRECTIVES} from 'angular2/router';

@Component({
    templateUrl: './chart.component.html',
    directives: [ROUTER_DIRECTIVES]
})

export class AdChartComponent implements OnInit {
    pageTitle:string = 'Post Detail';
    fromDate:string = '2016-08-20';
    toDate:string = '2016-09-10';
    dataPoints:IDataPoint[];
    errorMessage:string;

    constructor(private _chartService:ChartService) {
    }

    //TODO: add Date format validation
    plotData() {
        this._chartService.getDataPoints(this.fromDate, this.toDate).subscribe(
            dp => this.dataPoints = dp,
            error => this.errorMessage = <any>error
        )
    }

    ngOnInit() {

    }
}
