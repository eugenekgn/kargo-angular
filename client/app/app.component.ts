import {Component} from 'angular2/core';
import {HTTP_PROVIDERS} from 'angular2/http';
import 'rxjs/Rx'; // Load all features
import {ROUTER_PROVIDERS, ROUTER_DIRECTIVES, RouteConfig} from 'angular2/router';

import {PostService} from './posts/post.service';
import {HomeComponent} from "./home/home.component";
import {PostListComponent} from './posts/post-list.component';
import {PostDetailComponent} from './posts/post-detail.component';
import {AdChartComponent} from './chart/chart.component'
import {ChartService} from "./chart/chart.service";

@Component({
    selector: 'post-app',
    templateUrl: 'layout/page.component.html',
    directives: [
        ROUTER_DIRECTIVES,
        PostListComponent,
        AdChartComponent
    ],
    providers: [
        PostService,
        ChartService,
        HTTP_PROVIDERS,
        ROUTER_PROVIDERS
    ]
})

@RouteConfig([
    {path: '/home', name: 'Home', component: HomeComponent},
    {path: '/posts', name: 'Posts', component: PostListComponent, useAsDefault: true},
    {path: '/post/:id', name: 'PostDetail', component: PostDetailComponent},
    {path: '/chart', name: 'Chart', component: AdChartComponent}
])

export class AppComponent {
    pageTitle:string = 'Posts';
}