module.exports = function () {
  var serverPath = 'server',
    clientPath = 'client',
    buildPath = 'dist',
    vendorPath = 'node_modules';

  var app = clientPath + '/app';

  var server = {
    tsConfig: serverPath + '/tsconfig.json',
    tsAll: serverPath + '/**/*.ts',
    serverJs: 'server.js',
    dest: buildPath
  };

  var client = {
    tsConfig: clientPath + '/tsconfig.json',
    tsAll: clientPath + '/**/*.ts',
    libsPath: buildPath + '/libs',
    index: clientPath + '/index.html',
    dest: buildPath,
    templatesPath: buildPath + '/templates',
    templatesAll: app + '/**/*.html'
  };

  var clientJs = [
    'angular2/es6/dev/src/testing/shims_for_IE.js',
    'angular2/bundles/angular2-polyfills.js',
    'es6-shim/es6-shim.js',
    'systemjs/dist/system-polyfills.js',
    'systemjs/dist/system.src.js',
    'rxjs/bundles/Rx.js',
    'angular2/bundles/http.dev.js',
    'angular2/bundles/angular2.dev.js',
    'angular2/bundles/router.dev.js'];

  return {
    vendorPath: vendorPath,
    server: server,
    client: client,
    clientJs: clientJs
  }
};