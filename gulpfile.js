var gulp = require('gulp');
var path = require('path');
var sourceMaps = require('gulp-sourceMaps');
var ts = require('gulp-typescript');
var del = require('del');
var concat = require('gulp-concat');
var runSequence = require('run-sequence');
var embedTemplates = require('gulp-angular-embed-templates');
var config = require('./gulpfile.config.js')();

gulp.task('default', ['build']);

gulp.task('build', function(callback) {
  runSequence('clean', 'server', 'client', 'app', callback);
});

gulp.task('server', function() {
  var tsProject = ts.createProject(config.server.tsConfig);
  var tsResult = gulp.src(config.server.tsAll)
    .pipe(sourceMaps.init())
    .pipe(ts(tsProject));

  return tsResult.js
    .pipe(concat(config.server.serverJs))
    .pipe(sourceMaps.write())
    .pipe(gulp.dest(config.server.dest))
});

gulp.task('client', function() {
  var mappedPaths = config.clientJs.map(file => {
    return path.resolve(config.vendorPath, file)
  });

  var mJsFiles = gulp.src(mappedPaths, {base: config.vendorPath})
    .pipe(gulp.dest(config.client.libsPath));

  var index = gulp.src(config.client.index)
    .pipe(gulp.dest(config.client.dest));

  return [mJsFiles, index];
});

gulp.task('app', function() {
  var tsProject = ts.createProject(config.client.tsConfig);
  var tsResult = gulp.src(config.client.tsAll)
    .pipe(embedTemplates({sourceType: 'ts'}))
    .pipe(sourceMaps.init())
    .pipe(ts(tsProject));

  return tsResult.js
    .pipe(sourceMaps.write())
    .pipe(gulp.dest(config.client.dest))
});

gulp.task('templates', function() {
  return gulp.src(config.client.templatesAll)
    .pipe(gulp.dest(config.client.templatesPath));
});

gulp.task('clean', function() {
  return del('dist')
});


